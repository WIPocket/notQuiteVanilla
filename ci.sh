curl -sL https://github.com/shyiko/jabba/raw/master/install.sh | bash && . ~/.jabba/jabba.sh
[ -s "$JABBA_HOME/jabba.sh" ] && source "$JABBA_HOME/jabba.sh"
jabba install adopt-openj9@1.8.0-282
jabba use adopt-openj9@1.8.0-282

make download-server
make test-server
